from setuptools import setup

setup(name='smartmeter',
      version='0.1.1',
      description='Python package to retrieve smart meter data via serial port.',
      url='https://gitlab.com/paalders/smartmeter',
      author='Paalders',
      author_email='p.aalders@dotnetengineer.nl',
      license='MIT',
      packages=['smartmeter'],
      install_requires=[
          'pyserial',
      ],
      zip_safe=False)