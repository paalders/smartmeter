import requests
import json
import persistqueue
import logging
import logging.handlers
import sys
import configparser 
import time
from threading import Thread

# Load the configuration file
config = configparser.ConfigParser()
config.read('config.ini')

# Deafults
LOG_FILENAME = config['smartmeter']['logfilename']
LOG_LEVEL = logging.DEBUG  # Could be e.g. "DEBUG" or "WARNING"

# Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
# Give the logger a unique name (good practice)
logger = logging.getLogger("smartmeter.datasender")
logger.setLevel(LOG_LEVEL)
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
formatter = logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)

def worker():
    while True:
        data = queue.get()

        response = requests.post(
            config['datasender']['api-url'],
            headers={'X-AUTHKEY': config['datasender']['authkey']},
            auth=(config['datasender']['http-user'],config['datasender']['http-password']),
            data=data
        )

        if response.status_code != 201:
            logger.error('Error: {}'.format(response.text))
            queue.nack(data)
        else:
            logger.info('Created: {}'.format(json.loads(response.text)))
            queue.ack(data)

queue = persistqueue.FIFOSQLiteQueue(path=config['smartmeter']['queue'], multithreading=True)

while True:
    queue_length = len(queue)
    logger.debug("Queue length: {0}".format(queue_length))

    if queue_length > 0:
        number_of_threads = 1
        if queue_length > 5000:
            number_of_threads = 20
        
        for i in range(number_of_threads):
            t = Thread(target=worker)
            t.daemon = True
            t.start()

    else:
        logger.info("Queue seems empty, nothing to send. Reassigning queue to be sure.")
        queue = persistqueue.FIFOSQLiteQueue(path=config['smartmeter']['queue'], multithreading=True)
        time.sleep(1)
