# Thanks to Ge Janssen for the details of the serial connection (https://github.com/gejanssen/slimmemeter-rpi)

import sys
import serial
import re
import datetime
import persistqueue
import configparser
import logging
import logging.handlers

# Load the configuration file
config = configparser.ConfigParser()
config.read('config.ini')

# Deafults
LOG_FILENAME = config['smartmeter']['logfilename']
LOG_LEVEL = logging.DEBUG  # Could be e.g. "DEBUG" or "WARNING"

# Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
# Give the logger a unique name (good practice)
logger = logging.getLogger("smartmeter.datasender")
logger.setLevel(LOG_LEVEL)
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
formatter = logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)


class SmartMeterData:
    """Generic data class for smartmeter data"""

    def __init__(self):
        self.total_consumed_power_tarif1 = 0.00
        self.total_consumed_power_tarif2 = 0.00
        self.total_produced_power_tarif1 = 0.00
        self.total_produces_power_tarif2 = 0.00
        self.current_tarif = 0
        self.current_consuming_power = 0.00
        self.current_producing_power = 0.00
        self.total_consumed_gas = 0.00

    def get_data_dictionary(self):
        dt = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        return  {
            'electricity_currently_delivered': self.current_consuming_power,
            'electricity_currently_returned': self.current_producing_power,
            'electricity_delivered_1': self.total_consumed_power_tarif1,
            'electricity_delivered_2': self.total_consumed_power_tarif2,
            'electricity_returned_1': self.total_produced_power_tarif1,
            'electricity_returned_2': self.total_produced_power_tarif2,
            'timestamp': dt,
            'extra_device_timestamp': dt,
            'extra_device_delivered': self.total_consumed_gas,
        }

    def __str__(self):
        return """Total consumed (tarif 1): {0}kWh
Total consumed (tarif 2): {1}kWh
Total produced (tarif 1): {2}kWh
Total produced (tarif 2): {3}kWh

Current tarif: {4}

Current power consumage: {5}kW
Current power production: {6}kW

Total gas consumed: {7}m3""".format(
            self.total_consumed_power_tarif1,
            self.total_consumed_power_tarif2,
            self.total_produced_power_tarif1,
            self.total_produces_power_tarif2,
            self.current_tarif,
            self.current_consuming_power,
            self.current_producing_power,
            self.total_consumed_gas
        )

class SmartMeter:

    def __init__(self, serialcon):
        self.serialcon = serialcon
        self.data = SmartMeterData()

    def readline(self):
        raw_data=''
        try:
            raw_data = self.serialcon.readline()
        except:
            sys.exit ("Can't read from serial port %s." % self.serialcon.name ) 

        data=str(raw_data.decode('UTF-8')).rstrip()

        return data

class LandisGyrE350(SmartMeter):
    """Class for the Landis Gyr E350 meter"""

    _identifier_start = "/XMX5L"
    _identifier_close = "!"

    def __init__(self, serialcon):
        super().__init__(serialcon)
        self.raw_data = ""

    def read(self):
        try:
            self.serialcon.open()
        except:
            sys.exit ("Error opening serial port %s." % self.serialcon.name)

        # Pull the lines until we have the start identifier
        waiting_for_start = True
        while waiting_for_start:
            line = self.readline()
            regex = re.compile("^{0}".format(self._identifier_start))
            if regex.match(line):
                waiting_for_start = False

            self.raw_data = line

        # Read all data until End tag
        reading = True
        while reading:
            line = self.readline()
            regex = re.compile("^{0}".format(self._identifier_close))
            if regex.match(line):
                reading = False

            self.raw_data = "{0}\n{1}".format(self.raw_data, line)

        try:
            self.serialcon.close()
        except:
            sys.exit ("Error closing serial port %s. Terminated." % self.serialcon.name)

    def parse(self):
        if self.raw_data:
            result = re.search(r'1.8.1\((.*)\*kWh', self.raw_data)
            self.data.total_consumed_power_tarif1 = float(result.group(1))

            result = re.search(r'1.8.2\((.*)\*kWh', self.raw_data)
            self.data.total_consumed_power_tarif2 = float(result.group(1))

            result = re.search(r'2.8.1\((.*)\*kWh', self.raw_data)
            self.data.total_produced_power_tarif1 = float(result.group(1))

            result = re.search(r'2.8.2\((.*)\*kWh', self.raw_data)
            self.data.total_produced_power_tarif2 = float(result.group(1))

            result = re.search(r'96.14.0\((.*)\)', self.raw_data)
            self.data.current_tarif = int(result.group(1))

            result = re.search(r'1.7.0\((.*)\*kW', self.raw_data)
            self.data.current_consuming_power = float(result.group(1))

            result = re.search(r'2.7.0\((.*)\*kW', self.raw_data)
            self.data.current_producing_power = float(result.group(1))

            result = re.search(r'\)\((.*)\*m3', self.raw_data)
            self.data.total_consumed_gas = float(result.group(1))

queue = persistqueue.FIFOSQLiteQueue(path=config['smartmeter']['queue'], multithreading=True)

serial_connection = serial.Serial()
serial_connection.baudrate = 115200
serial_connection.bytesize=serial.EIGHTBITS
serial_connection.parity=serial.PARITY_NONE
serial_connection.stopbits=serial.STOPBITS_ONE
serial_connection.xonxoff=0
serial_connection.rtscts=0
serial_connection.timeout=20
serial_connection.port= "/dev/ttyUSB0"

meter = LandisGyrE350(serial_connection)
meter.read()
meter.parse()

data = meter.data.get_data_dictionary()
queue.put(data)